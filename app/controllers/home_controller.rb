class HomeController < ApplicationController
  def index
    if params['city'].present?
      @weather_data = []
      dates_array = []
      begin
        result = JSON.parse(RestClient.get('http://api.openweathermap.org/data/2.5/forecast?q='+params['city'].split(',').first+','+params['city'].split(',').last.strip+'&APPID=c6e6416edc1be18a30aaaae318a224f0').body)
        @place_name = result['city']['name'] if result['city']
        result['list'].each do |whether_report|
          whether_date = whether_report['dt_txt'].to_date
          if !dates_array.include?(whether_date)
            dates_array.push(whether_date)
            @weather_data.push(whether_report)
          end
        end
      rescue StandardError => e
        @error = e.message
      end
    end
  end
end
