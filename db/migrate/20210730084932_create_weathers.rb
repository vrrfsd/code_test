class CreateWeathers < ActiveRecord::Migration[6.1]
  def change
    create_table :weathers do |t|
      t.string :temp_min
      t.string :temp_max
      t.string :pressure
      t.string :humidity
      t.datetime :date
      t.string :place_id

      t.timestamps
    end
  end
end
